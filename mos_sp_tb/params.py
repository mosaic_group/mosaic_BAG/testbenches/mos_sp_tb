#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import ParamsBase
from sal.simulation.simulation_output import *
from sal.testbench_params import *


@dataclass
class mos_sp_tb_params(TestbenchParamsBase):
    """
    Parameter class for mos_sp_tb

    Args:
    ----

    dut: DUT
        inherited from TestbenchParamsBase, encapsulates the DUT lib and cell

    sch_params: TestbenchSchematicParams
        inherited from TestbenchParamsBase, encapsulates schematic related parameters

    ...

    """

    vgs_start: float
    vgs_stop: float
    vgs_num: int

    vbs: float    # vbs: List[float]
    vb_dc: float

    sp_freq: float

    cfit_method: str

    @classmethod
    def builtin_outputs(cls) -> Dict[str, SimulationOutputBase]:
        """
        Builtin outputs are merged into the effective simulation params
        """
        return {
            'ibias': SimulationOutputCurrent(analysis='dc', signal='vg', terminal='/VD/MINUS'),
            'y11': SimulationOutputVoltage(analysis='sp', signal='y11', quantity=VoltageQuantity.V),
            'y12': SimulationOutputVoltage(analysis='sp', signal='y12', quantity=VoltageQuantity.V),
            'y13': SimulationOutputVoltage(analysis='sp', signal='y13', quantity=VoltageQuantity.V),
            'y21': SimulationOutputVoltage(analysis='sp', signal='y21', quantity=VoltageQuantity.V),
            'y22': SimulationOutputVoltage(analysis='sp', signal='y22', quantity=VoltageQuantity.V),
            'y23': SimulationOutputVoltage(analysis='sp', signal='y23', quantity=VoltageQuantity.V),
            'y31': SimulationOutputVoltage(analysis='sp', signal='y31', quantity=VoltageQuantity.V),
            'y32': SimulationOutputVoltage(analysis='sp', signal='y32', quantity=VoltageQuantity.V),
            'y33': SimulationOutputVoltage(analysis='sp', signal='y33', quantity=VoltageQuantity.V),
            'gm': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='gm'),
            'gmoverid': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='gmoverid'),
            'region': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='region'),
            'cgd': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='cgd'),
            'cgg': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='cgg'),
            'cds': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='cds'),
            'gds': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='gds'),
            'ron': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='ron'),
            'vth': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='vth'),
            'vdssat': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='vdssat'),
            'ft': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='ft'),
            'fug': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='fug'),
            'id': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='id'),
            'igeff': SimulationOutputDeviceParameter(analysis='dc', device='XDUT.XM.M0', parameter='igeff'),
        }

    @classmethod
    def defaults(cls) -> mos_sp_tb_params:
        return mos_sp_tb_params(
            dut=DUT.placeholder(),  # NOTE: Framework will inject DUT
            dut_wrapper_params=None,  # NOTE: generator can decide to use a wrapper
            sch_params=TestbenchSchematicParams(
                dut_conns=[],
                v_sources=[],
                i_sources=[],
                instance_cdf_parameters={}
            ),
            simulation_params=TestbenchSimulationParams(
                variables={},
                sweeps={},
                outputs={}
            ),
            vgs_start=0,
            vgs_stop=1.0,
            vgs_num=30,

            #vbs=[0.0, 0.15, 0.3, 0.45],
            vbs=0.0,
            vb_dc=0.0,

            sp_freq=1e6,
            cfit_method='average'
        )
